/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.betsafe.services;

import com.betsafe.db.DBTransactions;
import com.betsafe.utils.Logging;
import com.betsafe.utils.Props;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 *
 * @author antonio
 */
public class OddsConsumer {

	private final Logging logger;
	private final Props props;

	public OddsConsumer(Logging logger, Props props) {

		this.logger = logger;
		logger.info("Consumer:: Creating Odds Consumer");
		this.props = props;
	}
	
	public JSONObject readFile() throws Exception, IOException {

        String filepath = "/home/jameskamau/Downloads/betsafeoddslive2.json";
       JSONParser jsonParser = new JSONParser();
        JSONObject json = new JSONObject();
        try (FileReader reader = new FileReader(filepath)) {
        	
        	json = new JSONObject(jsonParser.parse(reader).toString());

        } catch (Exception e) {
        	e.printStackTrace();
            logger.error(e.toString());
        }
        return json;
    }
    

	public void fetchOdds(String apiAccount, String apiPassword, String eventsProgramCode, int bookmarkerId) throws Exception {

		OkHttpClient client = new OkHttpClient.Builder().connectTimeout(40, TimeUnit.SECONDS)
				.writeTimeout(40, TimeUnit.SECONDS).readTimeout(40, TimeUnit.SECONDS).build();

		String lastModified = "1900-01-01T00:00:00";
		

		ArrayList<HashMap<String, String>> lastModifiedList = DBTransactions
				.query("SELECT * FROM odds_last_modified_live ORDER " + "BY 1 DESC LIMIT 1");

		if (!lastModifiedList.isEmpty()) {
			lastModified = lastModifiedList.get(0).get("last_modified");
			logger.info("Last odds modification date is " + lastModified);
		}

		MediaType mediaType = MediaType.parse("application/json");
		RequestBody body = RequestBody.create(mediaType,
				"{\"APIAccount\":\"" + apiAccount + "\"," + "\"APIPassword\":\"" + apiPassword + "\","
						+ "\"EventsProgramCode\":\"" + eventsProgramCode + "\"," + "\"LanguageID\":\"2\","
						+ "\"DateLastModify\":\"" + lastModified + "\"," + "\"IDBookmaker\":\"" + bookmarkerId + "\"}");

		Request request = new Request.Builder()
				.url("https://" + Props.getAPIHostURL() + "/api/eventprogramlive/GetEventsProgram").post(body)
				.addHeader("content-type", "application/json").build();
		try {
			//Response response = client.newCall(request).execute();
			
			//String responseJSON = response.body().string();
			
			//JSONObject json = new JSONObject(responseJSON);
			JSONObject json=readFile();
			int resultCode = json.getInt("ResultCode");

			if (resultCode == 1) {

				String lastServerDate = json.getString("LastServerDate");
				JSONArray sports = json.getJSONArray("Sports");
				int noOfSports = sports.length();
				int sportId, order, matchesBatchControl, numMatches, parentMatchId, matchStatus, matchOrder,
						oddsBatchControl, numOdds, oddsId, subTypeId, oddsStatus, oddTypeId, numSubEvents;
				String sportName, sportSQL, competitionName,localCompetitionId, teams, startDate, name, shortName, oddKey;
				JSONArray  competitions, subEvents, matches, odds;
				JSONObject competitionObject, matchObject, oddObject, subEventObject;
				double oddValue, sbv;
				String betradarTimeStamp;
				ArrayList<String> matchesBatch, oddsBatch;

				for (int i = 0; i < noOfSports; i++) {

					JSONObject sportObject = sports.getJSONObject(i);

					sportId = sportObject.getInt("SportID");
					sportName = sportObject.getString("Sport");
					sportName = sportName.replace("'", "\\'");
					order = sportObject.getInt("Order");
					competitions = sportObject.getJSONArray("Groups");
					int numCompetitions = competitions.length();
					sportSQL = "INSERT INTO sport (`sport_name`,`created_by`, "
							+ "`priority`, `betradar_sport_id`) VALUES ('" + sportName + "'," + " 'ISOLUTIONS-ANTO', '"
							+ order + "', '" + sportId + "') " + "ON DUPLICATE KEY UPDATE `priority` = '" + order + "'";

					DBTransactions.update(sportSQL);

					for (int c = 0; c < numCompetitions; c++) {
						competitionObject = competitions.getJSONObject(c);
						competitionName = competitionObject.getString("Group");

						matches = competitionObject.getJSONArray("Events");
						numMatches = matches.length();
						matchesBatch = new ArrayList<>();
						matchesBatchControl = 0;

						localCompetitionId = String.valueOf(
								DBTransactions.getForeignKey("competition", "competition_name", competitionName));
						if (localCompetitionId == null) {
							continue;
						}

						for (int l = 0; l < numMatches; l++) {

							matchObject = matches.getJSONObject(l);

							parentMatchId = matchObject.getInt("EventID");
							matchStatus = matchObject.getInt("Status");
							teams = matchObject.getString("Event");
							startDate = matchObject.getString("StartDate");
							matchOrder = matchObject.getInt("Order");
							subEvents = matchObject.getJSONArray("SubEvents");
							numSubEvents = subEvents.length();

							if (teams.contains("-")) {
								logger.info(this.insertMatch(parentMatchId, teams, startDate, matchStatus,
										localCompetitionId, matchOrder));
								matchesBatch.add(this.insertMatch(parentMatchId, teams, startDate, matchStatus,
										localCompetitionId, matchOrder));
								oddsBatch = new ArrayList<>();
								oddsBatchControl = 0;
								for (int sb = 0; sb < numSubEvents; sb++) {
									subEventObject = subEvents.getJSONObject(sb);
									odds = subEventObject.getJSONArray("Odds");

									numOdds = odds.length();

									for (int m = 0; m < numOdds; m++) {

										oddObject = odds.getJSONObject(m);

										oddsId = oddObject.getInt("OddsID");
										name = oddObject.getString("OddsClass");
										name = name.replace("'", "\\'");
										shortName = oddObject.getString("OddsClassCode");
										shortName = shortName.replace("'", "\\'");
										sbv = oddObject.getDouble("HND");
										oddKey = oddObject.getString("OddsType");
										oddKey = oddKey.replace("'", "\\'");
										oddValue = oddObject.getDouble("Odds");
										betradarTimeStamp = oddObject.getString("StartDate").replace("T", " ");
										DBTransactions.update("INSERT IGNORE INTO" + " sub_type (`subtype_name`) "
												+ "VALUES('" + name + "')");

										subTypeId = this.getSubTypeFromOddClass(name);
										oddsStatus = oddObject.getInt("Status");
										oddTypeId = oddObject.getInt("OddsTypeID");

										if (oddsStatus == 1) {

											oddsBatch.add(this.insertOdds(oddKey, oddValue, sbv, oddsStatus,
													parentMatchId, oddTypeId, oddsId, subTypeId, betradarTimeStamp));

											oddsBatchControl++;

											if (oddsBatchControl == 300 || m == (numOdds - 1)) {

												if (!DBTransactions.updateMultiple(oddsBatch)) {
													logger.error("ERROR IN ODDS BATCH" + " WITH THESE QUERIES\n");
													oddsBatch.stream().forEach((query) -> logger.error(query));
												} else {
													logger.info("Executed odds batch with " + oddsBatchControl + " "
															+ "records successfully");
												}
												oddsBatch.clear();
												oddsBatchControl = 0;
											}
										}

									}

									matchesBatchControl++;

									if (matchesBatchControl == 50 || l == (numMatches - 1)) {

										if (!DBTransactions.updateMultiple(matchesBatch)) {
											logger.error("ERROR IN MATCHES BATCH" + " WITH THESE QUERIES\n");
											matchesBatch.stream().forEach((query) -> logger.error(query));
										} else {
											logger.info("Executed batch with " + matchesBatchControl + " "
													+ "records successfully");
										}
										//matchesBatch.clear();
										matchesBatchControl = 0;
									}
								}
							}
						}
					}

				}
				this.updateOddsLastModified(lastServerDate);
			} else {
				logger.error("Failed to fetch odds Result Code " + resultCode + " Result DESC "
						+ json.getString("ResultDescription"));
			}

			logger.info("Successfully wrote odds records to the database");
			System.exit(0);
		} catch (IOException | JSONException ee) {
			logger.error("Exception within the fetch odds method " + ee);
		}
	}

	public String insertMatch(int parentMatchID, String match, String startDate, int status, String competitionID,
			int order) {

		String home = match.substring(0, match.indexOf("-") - 1);
		String away = match.substring(match.indexOf("-") + 1);

		home = home.replace("'", "\\'").trim();
		away = away.replace("'", "\\'").trim();

		String query = "INSERT INTO `live_match` (`parent_match_id`,`home_team`,`away_team`, "
				+ "`start_time`,`game_id`,`competition_id`,`status`,`instance_id`,"
				+ "`bet_closure`,`created_by`, `completed`,`priority`) " + " VALUES('" + parentMatchID + "', '" + home
				+ "', '" + away + "'," + "'" + startDate.replace("T", " ") + "', '100',  '" + competitionID + "',"
				+ " '" + status + "', '1', '" + startDate.replace("T", " ") + "'," + "'ISOLUTIONS-ANTO', '0', '"
				+ order + "') ON DUPLICATE KEY " + "UPDATE `start_time` = '" + startDate.replace("T", " ") + "', "
				+ "`bet_closure` = '" + startDate.replace("T", " ") + "'," + "`priority` = '" + order + "'";

		return query;

	}

	public int getSubTypeFromOddClass(String OddsClass) {

		return DBTransactions.getForeignKey("sub_type", "subtype_name", OddsClass);
	}

	public String insertOdds(String oddKey, double odds, double sbv, int status, int parentMatchId, int oddsTypeID,
			int betradarOddsID, int subTypeId, String betradarTimeStamp) {

		oddKey = oddKey.replace("'", "\\'");
		return "INSERT INTO live_odds (`parent_match_id`,`sub_type_id`," + "`odd_key`, `odd_value`,`special_bet_value`,"
				+ "`betradar_odd_id`,`score`,`active`,`odd_active`,`betradar_timestamp`) VALUES ('" + parentMatchId
				+ "', " + "'" + subTypeId + "','" + oddKey + "', '" + odds + "'," + "'" + sbv + "','" + betradarOddsID
				+ "', '0:0', '1','1','" + betradarTimeStamp + "') ON DUPLICATE KEY UPDATE " + "`odd_value` = '" + odds
				+ "', betradar_odd_id = " + "'" + betradarOddsID + "'";
	}

	public void updateOddsLastModified(String lastModified) {

		String query = "INSERT INTO odds_last_modified VALUES(NULL, " + "'" + lastModified + "')";
		DBTransactions.update(query);
	}
}
