/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.betsafe;

import java.io.FileReader;
import java.io.IOException;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.betsafe.services.OddsConsumer;
import com.betsafe.utils.Logging;
import com.betsafe.utils.Props;

/**
 *
 * @author antonio
 */
public class Betsafe {

    static Props props = new Props(); 
    static Logging logger = new Logging(); 
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        String apiAccount = Props.getAPIAccount();
        String apiPassword = Props.getAPIPassword();
        String eventsProgramCode = Props.getEventsProgramCode();
        int bookmarkerId = Props.getBookmarkerId();
            
        OddsConsumer oddsConsumer = new OddsConsumer(logger,props);       
        try {
			oddsConsumer.fetchOdds(apiAccount, apiPassword, eventsProgramCode,
			        bookmarkerId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }   
    
    
    
}